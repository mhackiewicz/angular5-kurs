import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ContactService } from '../contact.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact-item',
  templateUrl: './contact-item.component.html',
  styleUrls: ['./contact-item.component.css']
})
export class ContactItemComponent implements OnInit {
  @Input() contact:any; 
  @Output() reloadList = new EventEmitter<string>();
  constructor(private service: ContactService,private router: Router) { }

  ngOnInit() {
  }

  delete(id){
      this.service.deleteContact(id).subscribe(data => {            
        this.reloadList.emit();
      });
  }

}
