import { Component, OnInit } from '@angular/core';
import { ContactService } from '../contact.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItemComponent implements OnInit {
  name:string = "";
  phone:string = "";
  email:string = "";

  constructor(private service: ContactService, private router: Router) { }

  ngOnInit() {
  }
  add() {
    let newContact = {
      name: this.name,
      phone: this.phone,
      email: this.email
    }
    this.service.addContact(newContact).subscribe(data => {    
      this.router.navigate(['']); 
    });
 
  }

}
