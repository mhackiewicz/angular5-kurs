import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ContactService {
  private url = 'http://localhost:3000/contacts'
  constructor(private http: HttpClient) { }

  getContacts(): Observable<any> {
    return this.http.get<any>(this.url);
  }

  addContact(contact: any) {
    return this.http.post(this.url, contact); 
  } 

  deleteContact(id:number){
    return this.http.delete(`${this.url}/${id}`); 
  }
}
