import { Component, OnInit } from '@angular/core';
import { ContactService } from '../contact.service';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {
  contactList: any;
  constructor(private service:ContactService) { }

  getData() {
    this.service.getContacts().subscribe(data => {
      this.contactList = data;
      console.log(this.contactList)
    });
  }

  ngOnInit() {
    this.getData();
  }

}
